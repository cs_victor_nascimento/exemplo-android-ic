package br.com.concretesolutions.exemplo.ic;

import android.test.ActivityInstrumentationTestCase2;

public class MainActivityTest extends
		ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity mainActivity;

	public MainActivityTest() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		mainActivity = getActivity();
	}

	public void testHelloWorld() {
		assertNotNull(mainActivity);
	}
}